var readline = require('readline');

var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

var HanoiGame = function () {
    this.stacks = [[1, 2, 3, 4], [], []];
};

HanoiGame.prototype.isWon = function () {
    if (this.stacks[0].length === 0 && this.stacks[1].length === 0) {
      return true;
    } else {
      return false;
    }
};

HanoiGame.prototype.isValidMove = function (startTowerIdx, endTowerIdx) {
    var startTower = this.stacks[startTowerIdx];
    var endTower = this.stacks[endTowerIdx];

    if (endTower.length === 0 || startTower[startTower.length - 1] > endTower[endTower.length - 1]) {
        return true;
    } else {
        return false;
    }
};

HanoiGame.prototype.move = function (startTowerIdx, endTowerIdx) {

    if (this.isValidMove(startTowerIdx, endTowerIdx)) {
      console.log("valid move");
        this.stacks[endTowerIdx].push(this.stacks[startTowerIdx].pop());
    }

    if (this.isWon()) {
      completionCallback();
    } else {
      this.run();
    }
};

HanoiGame.prototype.print = function () {
    console.log(JSON.stringify(this.stacks));
};

HanoiGame.prototype.promptMove = function (callback) {
  this.print();

  var startTowerIdx = null;
  var endTowerIdx = null;
  var that = this;

  reader.question("Move from stack number?", function (answer) {
    startTowerIdx = answer;

    reader.question("Move to stack number?", function (answer) {
      endTowerIdx = answer;

      callback.call(that, startTowerIdx, endTowerIdx);
    });
  });
};

HanoiGame.prototype.run = function(completionCallback) {
    this.promptMove(this.move);

};

var completionCallback = function () {
    console.log("You won! Hooraaaaayy...... ");
};



var game = new HanoiGame();

game.run(completionCallback);